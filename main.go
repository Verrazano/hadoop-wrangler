package main

import (
  "fmt"
  "net/http"
  "os"
  "strings"
  "encoding/json"
  "gitlab.com/Verrazano/hadoop-wrangler/hadoopconf"
)

var hadoopConf hadoopconf.HadoopConf
var hostname string

const DEFAULT_NN_HTTP_PORT = "50070"
const DEFAULT_RM_HTTP_PORT = "8088"

type ServerInfo struct {
  Address string
  Port string
}

type NameNodeStatus struct {
  Status []struct {
    HostAndPort string
  } `json:"beans"`
}

type NameNodeInfo struct {
  Info []struct {
    LiveNodes string
  } `json:"beans"`
}
func JMXQuery(server ServerInfo, query string) (*http.Response, error) {
  uri := fmt.Sprintf("http://%s:%s/jmx?qry=%s",
    server.Address, server.Port, query)
  resp, err := http.Get(uri)

  return resp, err
}

func FindNameNode() (ServerInfo, error) {
  // Build a list of addresses to check
  var addrs []ServerInfo

  if hadoopConf != nil {
    if addr, ok := hadoopConf["dfs.namenode.http-address"]; ok {
      s := strings.Split(addr, ":")
      addrs = append(addrs, ServerInfo{
        Address: s[0],
        Port: s[1],
      })
    }
  }

  addrs = append(addrs, ServerInfo{
    Address: hostname,
    Port: DEFAULT_NN_HTTP_PORT,
  })

  // Check all the addresses in the list
  for _, addr := range addrs {
    namenode, err := CheckForNameNode(addr)
    if err == nil {
      return namenode, nil
    }
  }

  return ServerInfo{}, fmt.Errorf("couldn't find namenode")
}

func CheckForNameNode(addr ServerInfo) (ServerInfo, error) {
  resp, err := JMXQuery(addr, "Hadoop:service=NameNode,name=NameNodeStatus")
  if err != nil {
    return ServerInfo{}, err
  }

  var status NameNodeStatus
  json.NewDecoder(resp.Body).Decode(&status)
  host := status.Status[0].HostAndPort
  address := strings.Split(host, ":")[0]

  return ServerInfo{
    Address: address,
    Port: addr.Port,
  }, nil
}

func FindResourceManager() (ServerInfo, error) {
  // Build list of addresses to check
  var addrs []ServerInfo

  if hadoopConf != nil {
    if addr, ok := hadoopConf["yarn.resourcemanager.webapp.address"]; ok {
      s := strings.Split(addr, ":")
      addrs = append(addrs, ServerInfo{
        Address: s[0],
        Port: s[1],
      })
    }
  }

  addrs = append(addrs, ServerInfo{
    Address: hostname,
    Port: DEFAULT_RM_HTTP_PORT,
  })

  // Check all the addresses in the list
  for _, addr := range addrs {
    resourcemanager, err := CheckForResourceManager(addr)
    if err == nil {
      return resourcemanager, nil
    }
  }

  return ServerInfo{}, fmt.Errorf("couldn't find resourcemanager")

}

func CheckForResourceManager(addr ServerInfo) (ServerInfo, error) {
  _, err := JMXQuery(addr, "Hadoop:service=ResourceManager,name=RMNMInfo")
  if err != nil {
    return ServerInfo{}, err
  }

  return addr, nil
}

func GetDataNodes(namenode ServerInfo) ([]ServerInfo, error) {
  var datanodes []ServerInfo

  resp, err := JMXQuery(namenode, "Hadoop:service=NameNode,name=NameNodeInfo")
  if err != nil {
    return nil, err
  }

  var info NameNodeInfo
  json.NewDecoder(resp.Body).Decode(&info)

  var nodes map[string]struct{
    Address string `json:"infoAddr"`
  }
  json.Unmarshal([]byte(info.Info[0].LiveNodes), &nodes)

  for key, value := range nodes {
    host := strings.Split(key, ":")[0]
    port := strings.Split(value.Address, ":")[1]

    datanodes = append(datanodes, ServerInfo{
      Address: host,
      Port: port,
    })
  }

  return datanodes, nil
}

func GetNodeManagers(resourcemanager ServerInfo) ([]ServerInfo, error) {
  var nodemanagers []ServerInfo

  resp, err := JMXQuery(resourcemanager, "Hadoop:service=ResourceManager,name=RMNMInfo")
  if err != nil {
    return nil, err
  }

  var info struct{
    Info []struct {
      LiveNodeManagers string
    } `json:"beans"`
  }
  json.NewDecoder(resp.Body).Decode(&info)

  var nodes []struct{
	NodeHTTPAddress string
  }
  json.Unmarshal([]byte(info.Info[0].LiveNodeManagers), &nodes)

  for _, node := range nodes {
    s := strings.Split(node.NodeHTTPAddress, ":")
    nodemanagers = append(nodemanagers, ServerInfo{
      Address: s[0],
      Port: s[1],
    })
  }

  return nodemanagers, nil
}

func main() {
  fmt.Println("Starting Hadoop Wrangler...")

  hadoopConf, _ = hadoopconf.LoadFromEnvironment()
  if hadoopConf == nil {
    hadoopConf, _ = hadoopconf.Load("/etc/hadoop/conf")
    if hadoopConf == nil {
      fmt.Println("Can't find hadoop configs")
    }
  }

  var err error
  hostname, err = os.Hostname()
  if err != nil {
    fmt.Println(err)
  }

  namenode, err := FindNameNode()
  if err != nil {
    fmt.Println(err)
  } else {
    fmt.Printf("NameNode: %s:%s\n", namenode.Address, namenode.Port)
    datanodes, _ := GetDataNodes(namenode)
    fmt.Printf("DataNodes (%d)\n", len(datanodes))
    for _, dn := range datanodes {
      fmt.Printf("%s:%s\n", dn.Address, dn.Port)
    }
  }

  resourcemanager, err := FindResourceManager()
  if err != nil {
    fmt.Println(err)
  } else {
    fmt.Printf("ResourceManager: %s:%s\n", resourcemanager.Address, resourcemanager.Port)
    nodemanagers, _ := GetNodeManagers(resourcemanager)
    fmt.Printf("NodeManagers (%d)\n", len(nodemanagers))
    for _, nm := range nodemanagers {
      fmt.Printf("%s:%s\n", nm.Address, nm.Port)
    }
  }

}
